#  Gpcrocker_Skeleton Module
Magento 2 Gpcrocker Skeleton Module

## Description

The Skeleton module is responsible for a quick module start

Explain functionality here

## Installation

- Place the Module inside Magento2 `app/code` directory
- Run `bin/magento module:enable Gpcrocker_Skeleton --clear-static-content`
- Run `php bin/magento setup:upgrade`
- In Production mode Run `bin/magentobin/magento setup:static-content:deploy && bin/magento setup:di:compile`
- Finally run `php bin/magento clean:cache`

## Compatibility

Tested on Magento 2.3.5 and PHP 7.3

## License

Each source file included in this distribution is licensed under OSL 3.0

Open Software License (OSL 3.0). Please see LICENSE.txt for the full text of the OSL 3.0 license

## Contributor

- Graham Crocker