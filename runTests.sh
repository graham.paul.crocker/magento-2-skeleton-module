#!/usr/bin/env bash

MAGE_ROOT=../../../..

if [ -f ${MAGE_ROOT}/vendor/bin/phpunit ]; then
    php ${MAGE_ROOT}/vendor/bin/phpunit -c ${MAGE_ROOT}/dev/tests/unit/phpunit.xml.dist Test/*/*/* --testdox
else
    echo 'Run composer install'
fi

